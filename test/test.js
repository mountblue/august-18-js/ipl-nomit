var expect = require('chai').expect;
var matches = require('../script/main.js')
describe('Matches Played Per Year',function(){
it('should return number of matches played per year ',function(){
    var Input=[
        {season:2018},
        {season:2016}
    ]
    var Output={
        2016:1,
        2018:1
    }
    expect(matches.matchesPlayedPerYear(Input)).to.deep.equal(Output)
})
})
describe('Matches Won Per Team Per Year ',function(){
    it('should return count of matches won per team per year',function(){
        var Input=[
            {season:2018,winner:'sunriser'},
            {season:2018,winner:'kolkata'},
            {season:2017,winner:'sunriser'},
            {season:2017,winner:''}
        ]
        var Output={
            sunriser:[1,1],
            kolkata:[0,1]
        }
        expect(matches.matchesWonPerTeamPerYear(Input)).to.deep.equal(Output)
    })
})
describe('extra Run Conceded Per Team In 2016',function(){
    it('should return count of extra run per team',function(){
        var matchesData=[
             {season:2016,id:1},
             {season:2016,id:2},
             {season:2016,id:3}
        ]
        var deliveriesData=[
            {match_id:3,bowling_team:'sunriser',extra_runs:2},
            {match_id:3,bowling_team:'sunriser',extra_runs:0},
            {match_id:3,bowling_team:'sunriser',extra_runs:4}
        ]
        var output={
            sunriser:6
        }
        expect(matches.extraRunConcededPerTeamIn2016(deliveriesData,matchesData)).to.deep.equal(output)
    })
})
describe('top Economical Bowlers',function(){
    it('should pass the test',function(){
        var matchesData=[
            {season:2015,id:1},
            {season:2015,id:2},
            {season:2015,id:3}
       ]
       var deliveriesData=[
           {match_id:1,bowler:'Shivam Sharma',total_runs:4},
           {match_id:1,bowler:'Shivam Sharma',total_runs:2},
           {match_id:1,bowler:'Shivam Sharma',total_runs:4},
           {match_id:1,bowler:'Shivam Sharma',total_runs:4},
           {match_id:2,bowler:'UT Pandey',total_runs:4},
           {match_id:2,bowler:'UT Pandey',total_runs:4},
           {match_id:3,bowler:'UT Pandey',total_runs:4},
           {match_id:3,bowler:'UT Pandey',total_runs:4},
           {match_id:1,bowler:'Shivam Sharma',total_runs:4},
           {match_id:1,bowler:'Shivam Sharma',total_runs:3},
       ]
       var output={
           'Shivam Sharma':21,
           'UT Pandey':24
       }
       expect(matches.topEconomicalBowlerIn2015(deliveriesData,matchesData)).to.deep.equal(output)
    })
})