function extraRun() {
  $.getJSON('./json/extraRunConcededPerTeamIn2016.json', function(datajson) {
    var extraRunPerTeam = [];
    extraRunPerTeam = Object.entries(datajson);
    Highcharts.chart('container', {
      chart: {
        type: 'column'
      },
      title: {
        text: 'Extra Run Conceded Per Team In 2016'
      },
      subtitle: {
        text:
          'Source: <a href="https://www.kaggle.com/manasgarg/ipl/version/5">Indian Premier League</a>'
      },
      xAxis: {
        type: 'category',
        labels: {
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif'
          }
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Extra Runs'
        }
      },
      legend: {
        enabled: false
      },
      series: [
        {
          data: extraRunPerTeam,
          dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y:.1f}',
            y: 10,
            style: {
              fontSize: '13px',
              fontFamily: 'Verdana, sans-serif'
            }
          }
        }
      ]
    });
  });
}
