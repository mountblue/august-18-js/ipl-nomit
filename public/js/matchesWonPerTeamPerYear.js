function matchesWon() {
    $.getJSON("./json/matchesWonPerTeamPerYear.json", function (datajson) {
       var teamWonMatches=Object.entries(datajson);
       var matchesWonPerTeam;
       matchesWonPerTeam=teamWonMatches.map(function(count){
           var wonMatches={};
           wonMatches.name=count[0];
           wonMatches.data=count[1];
           return wonMatches
       })
        Highcharts.chart('container', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Matches Won Per Team Per Year'
            },
            subtitle: {
                text: 'Source: <a href="https://www.kaggle.com/manasgarg/ipl/version/5">Indian Premier League</a>'
            },
            xAxis: {
                categories: ['2008', '2009', '2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017']
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Matches Won Per Team Per Year'
                },
                stackLabels: {
                    enabled: false,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            legend: {
                align: 'right',
                x: -30,
                verticalAlign: 'top',
                y: 20,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                    }
                }
            },
            series: matchesWonPerTeam
        });
    });
}