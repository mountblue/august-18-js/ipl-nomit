function economicalBowlers() {
  $.getJSON('./json/economicalBowlers.json', (datajson) => {
    let topEconomicalBowler = [];
    topEconomicalBowler = Object.entries(datajson);
    Highcharts.chart('container', {
      chart: {
        type: 'column',
      },
      title: {
        text: 'Top Ten Economical Bowlers',
      },
      subtitle: {
        text: 'Source: <a href="https://www.kaggle.com/manasgarg/ipl/version/5">Indian Premier League</a>',
      },
      xAxis: {
        type: 'category',
        labels: {
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif',
          },
        },
      },
      yAxis: {
        min: 0,
        title: {
          text: ' Rating',
        },
      },
      legend: {
        enabled: false,
      },
      series: [
        {
          data: topEconomicalBowler,
          dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y:.1f}',
            y: 10,
            style: {
              fontSize: '13px',
              fontFamily: 'Verdana, sans-serif',
            },
          },
        },
      ],
    });
  });
}
