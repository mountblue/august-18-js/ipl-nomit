function matchesPlayed() {
  $.getJSON('./json/matchesPlayedPerYear.json', function(datajson) {
    var matchesPlayed = [];
    matchesPlayed = Object.entries(datajson);
    Highcharts.chart('container', {
      chart: {
        type: 'column'
      },
      title: {
        text: 'Matches Played Per Year'
      },
      subtitle: {
        text:
          'Source: <a href="https://www.kaggle.com/manasgarg/ipl/version/5">Indian Premier League</a>'
      },
      xAxis: {
        type: 'category',
        labels: {
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif'
          }
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Number of Matches '
        }
      },
      legend: {
        enabled: false
      },
      series: [
        {
          data: matchesPlayed,
          dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y:.1f}',
            y: 10,
            style: {
              fontSize: '13px',
              fontFamily: 'Verdana, sans-serif'
            }
          }
        }
      ]
    });
  });
}
