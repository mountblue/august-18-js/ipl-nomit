function matchesPlayedPerYear(matches) {
  var matchesPlayed = matches.reduce((matchesCount, data) => {
    if (matchesCount[data.season]) {
      matchesCount[data.season] += 1;
    } else {
      matchesCount[data.season] = 1;
    }
    return matchesCount;
  }, {});
  return matchesPlayed;
}
function matchesWonPerTeamPerYear(matches) {
  let matchesWonPerTeam = matches.reduce((matchesWon, data) => {
    if (!matchesWon[data.winner]) {
      if (data.winner) {
        matchesWon[data.winner] = matches.reduce((yearList, count) => {
          if (!yearList[count.season]) yearList[count.season] = 0;
          return yearList;
        }, {});
      }
    }
    if (data.winner) {
      if (matchesWon[data.winner][data.season]) {
        matchesWon[data.winner][data.season] += 1;
      } else matchesWon[data.winner][data.season] = 1;
    }
    return matchesWon;
  }, {});
  matchesWonPerTeam = Object.entries(matchesWonPerTeam);
  // console.log(result);
  matchesWonPerTeam = matchesWonPerTeam.reduce((matchesWon, data) => {
    matchesWon[data[0]] = Object.values(data[1]);
    return matchesWon;
  }, {});
  // console.log(matchesWonPerTeam);
  return matchesWonPerTeam;
}

function extraRunConcededPerTeamIn2016(deliveries, matches) {
  const matchList = matches.filter((data) => {
    if (data.season === 2016) return true;
  });
  const matchesId = matchList.map(data => data.id);
  // console.log(matchesId);
  const extraRunPerTeam = deliveries.reduce((teamExtraRun, data) => {
    // console.log(data.match_id, matchesId)
    if (matchesId.includes(data.match_id)) {
      if (teamExtraRun[data.bowling_team]) {
        teamExtraRun[data.bowling_team] += parseInt(data.extra_runs);
      } else teamExtraRun[data.bowling_team] = parseInt(data.extra_runs);
    }
    // console.log(teamExtraRun, 'teamExtraRun')
    return teamExtraRun;
  }, {});
  // console.log(extraRunPerTeam)
  return extraRunPerTeam;
}

function topEconomicalBowlerIn2015(deliveries, matches) {
  let matchesId = matches.filter((data) => {
    if (data.season === 2015) return true;
  });
  matchesId = matchesId.map(data => data.id);
  // console.log(matchesId)
  let bowlerRunsCount = deliveries.reduce((totalRunAndBall, data) => {
    if (matchesId.includes(data.match_id)) {
      if (!totalRunAndBall[data.bowler]) totalRunAndBall[data.bowler] = {};
      if (totalRunAndBall[data.bowler].balls) {
        totalRunAndBall[data.bowler].balls += 1;
      } else totalRunAndBall[data.bowler].balls = 1;
      if (totalRunAndBall[data.bowler].runs) {
        totalRunAndBall[data.bowler].runs += parseInt(data.total_runs);
      } else totalRunAndBall[data.bowler].runs = parseInt(data.total_runs);
    }
    return totalRunAndBall;
  }, {});
  // console.log(bowlerRunsCount);
  bowlerRunsCount = Object.entries(bowlerRunsCount);
  // console.log(bowlerRunsCount);
  let economicalBowler = bowlerRunsCount.reduce((runRate, data) => {
    runRate[data[0]] = data[1].runs / (data[1].balls / 6);
    return runRate;
  }, {});
  // console.log(economicalBowler);
  economicalBowler = Object.entries(economicalBowler);
  economicalBowler.sort((a, b) => a[1] - b[1]);
  // console.log(economicalBowler);
  let topEconomicalBowlers = economicalBowler.slice(0, 9);
  // console.log(topEconomicalBowlers);
  topEconomicalBowlers = topEconomicalBowlers.reduce((bowlerRunRate, data) => {
    bowlerRunRate[data[0]] = data[1];
    return bowlerRunRate;
  }, {});
  // console.log(topEconomicalBowlers);
  return topEconomicalBowlers;
}
module.exports = {
  matchesPlayedPerYear,
  matchesWonPerTeamPerYear,
  extraRunConcededPerTeamIn2016,
  topEconomicalBowlerIn2015,
};
