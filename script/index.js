const csvFilePath = '../csv/matches.csv';
const csvFilePathDeliveries = '../csv/deliveries.csv';
const csv = require('csvtojson');
const fs = require('fs');
const matches = require('./main.js');

let count;
csv()
  .fromFile(csvFilePath)
  .then((jsonMatches) => {
    count = matches.matchesPlayedPerYear(jsonMatches);
    fs.writeFile('../public/json/matchesPlayedPerYear.json', JSON.stringify(count), (err) => {
      if (err) throw err;
      console.log('the file is saved');
    });
    count = matches.matchesWonPerTeamPerYear(jsonMatches);
    fs.writeFile('../public/json/matchesWonPerTeamPerYear.json', JSON.stringify(count), (err) => {
      if (err) throw err;
      console.log('the file is saved');
    });
    csv()
      .fromFile(csvFilePathDeliveries)
      .then((jsonDeliveries) => {
        count = matches.extraRunConcededPerTeamIn2016(jsonDeliveries, jsonMatches);
        fs.writeFile(
          '../public/json/extraRunConcededPerTeamIn2016.json',
          JSON.stringify(count),
          (err) => {
            if (err) throw err;
            console.log('the file is saved');
          },
        );
        count = matches.topEconomicalBowlerIn2015(jsonDeliveries, jsonMatches);
        fs.writeFile('../public/json/economicalBowlers.json', JSON.stringify(count), (err) => {
          if (err) throw err;
          console.log('the file is saved');
        });
      });
  });
